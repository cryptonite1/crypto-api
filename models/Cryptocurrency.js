const mongoose = require("mongoose")
const Schema = mongoose.Schema;

//Modèle base de données
//Complet
const Cryptos = new Schema({
    _id : String,
    name: String,
    symbol: String,
    price: Number,
    date : String,
    versionKey: false
});

//Modèle a destination du graphique
/*const GraphSchema = new Schema({
    symbol : String,
    price: Number,
    date : String,
    
});*/


 const CryptosModel = mongoose.model("Cryptocurencies", Cryptos)
 //const Graph = mongoose.model("Graph", GraphSchema)

module.exports = {CryptosModel};
//module.exports = {Graph};