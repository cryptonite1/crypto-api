const express = require("express");
const router = express.Router();

const modelCrypto = require("../models/Cryptocurrency").CryptosModel;
//const graphCrypto = require("../models/Cryptocurrency").GraphSchema;


// Recuperer toutes les crypto
router.get("/getsAllCryptos", function (req, res) {
  modelCrypto.find({}, (error, posts) => {
    if (error) {
      res.status(400).json({error:error});
      return;
    }
    res.status(200).send({
      response: posts,
    });
  });
});

// Requete sur les 50 dernieres cryptos classé par valeur
router.get("/getsLastValue", function (req, res) {
  modelCrypto.find({}, (error, posts) => {
    if (error) {
      res.status(400).error(error);
      return;
    }
    res.status(200).send({
      response: posts,
    })
  }).sort({date: -1, price : -1}).limit(50);
});


// Recuperer crypto par symbole
router.get("/getsCharts/:symbol", (req, res) => {
  const symbol = req.params.symbol;
   modelCrypto.find({ symbol: symbol }, 'date price', (error, response) => {
    if (error) {
      res.status(400).json({ message: "Erreur, symbole indisponible"});
      return;
    }
    res.status(200).send({ response})
  });
});

module.exports = router;